package com.oauth;

import sun.misc.BASE64Encoder;

import java.net.URLEncoder;
import java.util.Date;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthMessage;
import net.oauth.signature.OAuthSignatureMethod;

public class TestOath {

	private String generateSignature(String signatueBaseStr, String oAuthConsumerSecret, String oAuthTokenSecret) {
		byte[] byteHMAC = null;
		try {
			Mac mac = Mac.getInstance("HmacSHA1");
			SecretKeySpec spec;
			if (null == oAuthTokenSecret) {
				String signingKey = encode(oAuthConsumerSecret) + '&';
				spec = new SecretKeySpec(signingKey.getBytes(), "HmacSHA1");
			} else {
				String signingKey = encode(oAuthConsumerSecret) + '&' + encode(oAuthTokenSecret);
				spec = new SecretKeySpec(signingKey.getBytes(), "HmacSHA1");
			}
			mac.init(spec);
			byteHMAC = mac.doFinal(signatueBaseStr.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BASE64Encoder().encode(byteHMAC);
	}

	private String encode(String value) {
		String encoded = "";
		try {
			encoded = URLEncoder.encode(value, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		String sb = "";
		char focus;
		for (int i = 0; i < encoded.length(); i++) {
			focus = encoded.charAt(i);
			if (focus == '*') {
				sb += "%2A";
			} else if (focus == '+') {
				sb += "%20";
			} else if (focus == '%' && i + 1 < encoded.length() && encoded.charAt(i + 1) == '7'
					&& encoded.charAt(i + 2) == 'E') {
				sb += '~';
				i += 2;
			} else {
				sb += focus;
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		TestOath oauth = new TestOath();
		
		  System.out.println(oauth.generateSignature("https://api.twitter.com/1.1/statuses/update.json", "cChZNFj6T5R0TigYB9yd1w",
		  "7588892-kagSNqWge8gB1WwE3plnFsJHAZVfxWD7Vb57p0b4"));
		 
		System.out.println(getOAuthURL("POST", "https://api.twitter.com/1.1/statuses/update.json", "kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw", "LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE", null));
	}

	public static String getOAuthURL(String method, String url, String oauth_consumer_key, String oauth_secret,
			String signature) {
		OAuthMessage om = new OAuthMessage(method, url, null);
		om.addParameter(OAuth.OAUTH_CONSUMER_KEY, oauth_consumer_key);
		if (signature == null)
			signature = OAuth.HMAC_SHA1;
		om.addParameter(OAuth.OAUTH_SIGNATURE_METHOD, signature);
		om.addParameter(OAuth.OAUTH_VERSION, "1.0");
		om.addParameter(OAuth.OAUTH_TIMESTAMP, new Long((new Date().getTime()) / 1000).toString());
		om.addParameter(OAuth.OAUTH_NONCE, UUID.randomUUID().toString());

		OAuthConsumer oc = new OAuthConsumer(null, oauth_consumer_key, oauth_secret, null);
		try {
			OAuthSignatureMethod osm = OAuthSignatureMethod.newMethod(signature, new OAuthAccessor(oc));
			osm.sign(om);
			url = OAuth.addParameters(url, om.getParameters());
			return url;
		} catch (Exception e) {
			 System.out.println(e.getMessage().toString());
			return null;
		}
	}

}
